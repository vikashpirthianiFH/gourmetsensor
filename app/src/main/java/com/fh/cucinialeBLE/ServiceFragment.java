package com.fh.cucinialeBLE;



import android.app.Fragment;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.ParcelUuid;

public abstract class ServiceFragment extends Fragment{
    public abstract BluetoothGattService getBluetoothGattService();
    public abstract ParcelUuid getServiceUUID();


    public int writeCharacteristic(BluetoothGattCharacteristic characteristic, int offset, byte[] value) {
        throw new UnsupportedOperationException("Method writeCharacteristic not overridden");
    };


    public void notificationsDisabled(BluetoothGattCharacteristic characteristic) {
        throw new UnsupportedOperationException("Method notificationsDisabled not overridden");
    };


    public void notificationsEnabled(BluetoothGattCharacteristic characteristic, boolean indicate) {
        throw new UnsupportedOperationException("Method notificationsEnabled not overridden");
    };


    public interface ServiceFragmentDelegate {
        void sendNotificationToDevices(BluetoothGattCharacteristic characteristic);
    }
}