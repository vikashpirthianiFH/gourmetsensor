package com.fh.cucinialeBLE;

import android.app.ListActivity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;


public class Peripherals extends ListActivity {

    private static final String[] PERIPHERALS_NAMES = new String[]{"Temperature"};
    public final static String EXTRA_PERIPHERAL_INDEX = "PERIPHERAL_INDEX";
    Peripheral mPeripheral = new Peripheral();
    TextView updatedMacAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peripherals_list);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
        /* layout for the list item */ android.R.layout.simple_list_item_1,
        /* id of the TextView to use */ android.R.id.text1,
        /* values for the list */ PERIPHERALS_NAMES);
        setListAdapter(adapter);

        Button updateMacAddrees = (Button)findViewById(R.id.button_macAddress);
        updatedMacAddress = (TextView)findViewById(R.id.text_macAddress);
        updateMacAddrees.setOnClickListener(mReadFileButtonListener);
    }
    private final View.OnClickListener mReadFileButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            updatedMacAddress.setText( Peripheral.getMacAddress());
        }
    };
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(this, Peripheral.class);
        intent.putExtra(EXTRA_PERIPHERAL_INDEX, position);
        startActivity(intent);
    }

}