package com.fh.cucinialeBLE;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class TemperatureServiceFragment extends ServiceFragment {

    int count = 0 ;
    private static final UUID sensorTemperatureServiceUUID = UUID.fromString("f000aa00-0451-4000-b000-000000000000");
    private static final int INITIAL_Temperature = 50;
    private static final UUID sensorTemperatureMonitorCharacteristicUUID = UUID.fromString("90530af2-3f8c-4cc7-8733-63e11525c0f3");

    private ServiceFragmentDelegate mDelegate;
    // UI
    private TextView temEditText1;
    private TextView temEditText2;
    private TextView temEditText3;
    private TextView temEditText4;
    private TextView temEditText5;
    private TextView temEditText6;

    List<String> list;

    private SeekBar mTempSeekBar1;
    private SeekBar mTempSeekBar2;
    private SeekBar mTempSeekBar3;
    private SeekBar mTempSeekBar4;
    private SeekBar mTempSeekBar5;
    private SeekBar mTempSeekBar6;

    private  Button mButtonReadFile;;

    private final OnSeekBarChangeListener mOnSeekBarChangeListener1 = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                count = progress;
                mTemperatureCharacteristic.setValue(count, BluetoothGattCharacteristic.FORMAT_UINT8, /* offset */ 0);
                temEditText1.setText(Integer.toString(count));

            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
    private final OnSeekBarChangeListener mOnSeekBarChangeListener2 = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                count = progress;
                mTemperatureCharacteristic.setValue(count, BluetoothGattCharacteristic.FORMAT_UINT8, /* offset */ 0);
                temEditText2.setText(Integer.toString(count));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
    private final OnSeekBarChangeListener mOnSeekBarChangeListener3 = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                mTemperatureCharacteristic.setValue(progress, BluetoothGattCharacteristic.FORMAT_UINT8, /* offset */ 0);
                temEditText3.setText(Integer.toString(count));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
    private final OnSeekBarChangeListener mOnSeekBarChangeListener4 = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                count = progress;
                mTemperatureCharacteristic.setValue(count, BluetoothGattCharacteristic.FORMAT_UINT8, /* offset */ 0);
                temEditText4.setText(Integer.toString(count));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
    private final OnSeekBarChangeListener mOnSeekBarChangeListener5 = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                count = progress;
                mTemperatureCharacteristic.setValue( count, BluetoothGattCharacteristic.FORMAT_UINT8, /* offset */ 0);
                temEditText5.setText(Integer.toString(count));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    private final OnSeekBarChangeListener mOnSeekBarChangeListener6 = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                count = progress;
                mTemperatureCharacteristic.setValue( count, BluetoothGattCharacteristic.FORMAT_UINT8, /* offset */ 0);
                temEditText6.setText(Integer.toString(count));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };


    private final OnClickListener mNotifyButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {


            mDelegate.sendNotificationToDevices(mTemperatureCharacteristic);
        }
    };
    private final OnClickListener mReadFileButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            int currentValue1 =  Integer.parseInt(list.get(0));
            mTemperatureCharacteristic.setValue( currentValue1, BluetoothGattCharacteristic.FORMAT_UINT8, /* offset */ 0);

        }
    };

    // GATT
    private BluetoothGattService mTemperatureService;
    private BluetoothGattCharacteristic mTemperatureCharacteristic;

    public TemperatureServiceFragment() {
        mTemperatureCharacteristic =
                new BluetoothGattCharacteristic(sensorTemperatureMonitorCharacteristicUUID, BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                        BluetoothGattCharacteristic.PERMISSION_READ);

        mTemperatureCharacteristic.addDescriptor(
                Peripheral.getClientCharacteristicConfigurationDescriptor());

        mTemperatureCharacteristic.addDescriptor(
                Peripheral.getCharacteristicUserDescriptionDescriptor(" Heighest Temperature is 100"));

        mTemperatureService = new BluetoothGattService(sensorTemperatureServiceUUID,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);
        mTemperatureService.addCharacteristic(mTemperatureCharacteristic);
    }


    // Lifecycle callbacks
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_temperature, container, false);

        list= new ArrayList<>();

        try {
            InputStreamReader is = new InputStreamReader(getActivity().getAssets().open("users.csv"));

            BufferedReader reader = new BufferedReader(is);

            reader.readLine();

            String line;
            while ((line = reader.readLine()) != null) {

                String[] line2 = line.split(","); // Splits the line up into a string array

                for (int i = 0 ; i < line2.length; i++){
                    list.add(line2[i]);
                }

            }
        } catch (IOException e) {
            Log.e("errorRead " , e.toString());
            e.printStackTrace();
        }

        temEditText1 = (TextView) view.findViewById(R.id.textView_temp1);
        temEditText2 = (TextView) view.findViewById(R.id.textView_temp2);
        temEditText3 = (TextView) view.findViewById(R.id.textView_temp3);
        temEditText4 = (TextView) view.findViewById(R.id.textView_temp4);
        temEditText5 = (TextView) view.findViewById(R.id.textView_temp5);
        temEditText6 = (TextView) view.findViewById(R.id.textView_temp6);


        mTempSeekBar1 = (SeekBar) view.findViewById(R.id.sensorSeekBar1);
        mTempSeekBar2 = (SeekBar) view.findViewById(R.id.sensorSeekBar2);
        mTempSeekBar3 = (SeekBar) view.findViewById(R.id.sensorSeekBar3);
        mTempSeekBar4 = (SeekBar) view.findViewById(R.id.sensorSeekBar4);
        mTempSeekBar5 = (SeekBar) view.findViewById(R.id.sensorSeekBar5);
        mTempSeekBar6 = (SeekBar) view.findViewById(R.id.sensorSeekBar6);

        mTempSeekBar1.setOnSeekBarChangeListener(mOnSeekBarChangeListener1);
        mTempSeekBar2.setOnSeekBarChangeListener(mOnSeekBarChangeListener2);
        mTempSeekBar3.setOnSeekBarChangeListener(mOnSeekBarChangeListener3);
        mTempSeekBar4.setOnSeekBarChangeListener(mOnSeekBarChangeListener4);
        mTempSeekBar5.setOnSeekBarChangeListener(mOnSeekBarChangeListener5);
        mTempSeekBar6.setOnSeekBarChangeListener(mOnSeekBarChangeListener6);


        Button notifyButton = (Button) view.findViewById(R.id.button_temperatureNotify);
        mButtonReadFile= (Button) view.findViewById(R.id.button_dataFromFile);
        mButtonReadFile.setOnClickListener(mReadFileButtonListener);
        notifyButton.setOnClickListener(mNotifyButtonListener);
        mTemperatureCharacteristic.setValue(INITIAL_Temperature, BluetoothGattCharacteristic.FORMAT_UINT8, /* offset */ 0);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mDelegate = (ServiceFragmentDelegate) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ServiceFragmentDelegate");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mDelegate = null;
    }

    public BluetoothGattService getBluetoothGattService() {
        return mTemperatureService;
    }

    @Override
    public ParcelUuid getServiceUUID() {
        return new ParcelUuid(sensorTemperatureServiceUUID);
    }


    @Override
    public void notificationsEnabled(BluetoothGattCharacteristic characteristic, boolean indicate) {
        if (characteristic.getUuid() != sensorTemperatureMonitorCharacteristicUUID) {
            return;
        }
        if (indicate) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(),"Notification Enabled ", Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    @Override
    public void notificationsDisabled(BluetoothGattCharacteristic characteristic) {
        if (characteristic.getUuid() != sensorTemperatureMonitorCharacteristicUUID) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "Notification Not Enabled", Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }



}